# Code and code blocks

Code and code blocks are treated in a special way

- the text is displayed as typed in so that carriage return and blanks would 
have the same effect as when using a typewriter
- the text is rendered in a monotype font
- if the code block has a special marker denoting the programming language then (depending upon the
    renderer) it is pretty-printed


**Verbatim**: Sometimes (especially in the TeX world) the word _verbatim_ is used to express that the text is
_typewriter_ text.


## Inline code

```
To get the kernel version enter the command `uname -v`.
```

To get the kernel version enter the command `uname -v`.


## Verbatim blocks


```
    ```
    this should appear the same way 
        like shown here
    ```
```


```
this should appear the same way 
    like shown here
```

## Code blocks

```
    ```python
    def show_answer():
        print('The answer is 42')

    ```
```



```python
def show_answer():
    print('The answer is 42')

```

