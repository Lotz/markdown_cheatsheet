# Emphasis

The Wikipedia article on [Emphasis](https://en.wikipedia.org/wiki/Emphasis_(typography)) says 

> In typography, **emphasis** is the strengthening of words in a text with a font in a different style from the rest of the text, to highlight them.

Here we show how simple text can be emphised in different ways.

## Italic

```
This is _italic_ 

or

This is also *italic*
```

This is _italic_ 

or

This is also *italic*


## Bold

```
This is **bold**
```

This is **bold**


## Mixing italic and bold

```
This is both **_italic and bold_**.
```

This is both **_italic and bold_**.


## Strikethrough


```
This is ~~strikethrough~~
```

This is ~~strikethrough~~
