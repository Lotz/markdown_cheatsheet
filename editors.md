# Editors

## Text Editors

When editing markdown texts a text editor should be used. 
**Don't ever use a word processor** when editing markdown.

There are multipurpose text editors which do support editing of markdown texts out of the box 
by offering syntax highlighting, and often more.

Here a list:

- Vim: Linux, Mac, Windows
- Emacs: Linux, Mac, Windows
- Atom: Linux, Mac, Windows
- Visual Studio: Linux, Mac, Windows
- Gedit: Linux
- Kedit: Linux
- Geany: Linux
- Sublime Text Editor (not free): Linux, Mac, Windows

## Special Markdown Editors

There exists special markdown editors which do support markdown as the multipurpose text editors, 
which additionally offer preview functionality and more.

## Online editors

Online editors which often offer the same functionality as a special markdown editor cannot be 
recommended if the markdown editing is done in a company environment. Otherwise, they may be 
a good alternative.


## What do I use?

Well, I am a Vim user and for markdown I additionally have installed
the [Vim Markdown](https://github.com/plasticboy/vim-markdown) package.


