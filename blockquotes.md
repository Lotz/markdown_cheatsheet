# Blockquotes

A blockquote is a section of text that is quoted from another source
and set off from the main text in a visually distinguisable way.

- Create a blockquote by adding a `>` in front of a paragraph
- The text inside a blockquote will be rendered as markdown
- Blockquotes can spanning paragraphs

Example:

```
>  This is **very important**.

>  This is less important.
```

and it looks like follows:

>  This is **very important**.

>  This is less important.


Example: Spanning paragraphs

```
>  “Space is big. You just won't believe how vastly, hugely, 
mind-bogglingly big it is. I mean, you may think it's a long way down
the road to the chemist's, but that's just peanuts to space.”
> 
> ― Douglas Adams, The Hitchhiker's Guide to the Galaxy 
```

>  “Space is big. You just won't believe how vastly, hugely, mind-bogglingly big it is. I mean, you may think it's a long way down the road to the chemist's, but that's just peanuts to space.”
> 
> ― Douglas Adams, The Hitchhiker's Guide to the Galaxy 


